<?php

/** @var Factory $factory */

use App\User;
use App\Workout;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Workout::class, function (Faker $faker) {
    $now = Carbon::parse($faker->dateTime);

    return [
        'name' => $faker->text(30),
        'description' => $faker->realText(120),
        'rating' => $faker->randomFloat(2, 0, 5),
        'user_id' => User::first()->id,
        'started_at' => $now,
        'ended_at' => $now->clone()->addMinutes(90),
    ];
});
