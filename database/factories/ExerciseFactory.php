<?php

/** @var Factory $factory */

use App\Exercise;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Exercise::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
    ];
});
