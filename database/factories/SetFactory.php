<?php

/** @var Factory $factory */

use App\Set;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Set::class, function (Faker $faker) {
    return [
        'order' => $faker->numberBetween(0, 5),
        'reps' => $faker->numberBetween(1, 20),
        'weight' => $faker->randomFloat(1, 10, 150),
    ];
});
