<?php

use App\Exercise;
use App\Set;
use App\Workout;
use Illuminate\Database\Seeder;

class WorkoutsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Workout::class, 50)->create()->each(function (Workout $workout) {
            foreach (Exercise::take(3)->inRandomOrder()->get() as $exercise) {
                $workout->exercises()->attach($exercise, ['order' => rand(0, 5)]);

                $exerciseWorkout = $workout->exercises()->find($exercise->id);
                $exerciseWorkout->pivot->sets()->createMany(factory(Set::class, 4)->make()->toArray());
            }
        });
    }
}
