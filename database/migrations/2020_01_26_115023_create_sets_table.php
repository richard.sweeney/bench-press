<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (Blueprint $table) {
            $table->unsignedBigInteger('exercise_workout_id');
            $table->smallInteger('order')->default(0);
            $table->smallInteger('reps');
            $table->float('weight');

            $table->foreign('exercise_workout_id')
                ->references('id')->on('exercise_workout')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sets', function (Blueprint $table) {
            $table->dropForeign(['exercise_workout_id']);
        });

        Schema::dropIfExists('sets');
    }
}
