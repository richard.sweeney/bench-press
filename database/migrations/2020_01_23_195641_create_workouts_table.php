<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workouts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->text('description')->nullable();
            $table->float('rating')
                ->nullable()->default(null);

            $table->unsignedBigInteger('user_id')
                ->nullable()->default(null);

            $table->dateTime('started_at');
            $table->dateTime('ended_at')
                ->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();

            $table->dropColumn('created_at');

            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workouts', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('workouts');
    }
}
