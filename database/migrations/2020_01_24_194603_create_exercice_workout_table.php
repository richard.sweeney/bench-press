<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExerciceWorkoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_workout', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('exercise_id');
            $table->unsignedBigInteger('workout_id');
            $table->smallInteger('order')->default(0);

            $table->foreign('exercise_id')
                ->references('id')->on('exercises');

            $table->foreign('workout_id')
                ->references('id')->on('workouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exercise_workout', function (Blueprint $table) {
            $table->dropForeign(['exercise_id']);
            $table->dropForeign(['workout_id']);
        });

        Schema::dropIfExists('exercise_workout');
    }
}
