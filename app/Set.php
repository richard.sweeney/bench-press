<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    public $timestamps = false;

    protected $fillable = ['order', 'reps', 'weight'];
}
