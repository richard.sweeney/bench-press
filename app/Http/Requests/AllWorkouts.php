<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AllWorkouts extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_ids' => 'string|nullable',
            'start_date' => 'date',
            'end_date' => 'date',
            'includes' => 'string|nullable',
        ];
    }
}
