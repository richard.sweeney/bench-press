<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreWorkout extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'numeric|required',
            'name' => 'string',
            'description' => 'string',
            'rating' => 'numeric',
            'exercises' => 'array',
        ];
    }
}
