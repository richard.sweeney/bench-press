<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllWorkouts;
use App\Http\Requests\StoreWorkout;
use App\Http\Resources\WorkoutResource;
use App\Repositories\WorkoutRepository;
use App\Workout;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WorkoutController extends Controller
{
    /**
     * @var WorkoutRepository
     */
    protected $repository;

    public function __construct(WorkoutRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(AllWorkouts $request): AnonymousResourceCollection
    {
        $workouts = $this->repository->index($request);

        return WorkoutResource::collection($workouts);
    }

    public function store(StoreWorkout $request): WorkoutResource
    {
        $workout = $this->repository->create($request);

        return new WorkoutResource($workout->load('exercises.sets'));
    }

    public function show(Workout $workout): WorkoutResource
    {
        return new WorkoutResource($workout->load('exercises'));
    }

    public function update(StoreWorkout $request, Workout $workout): WorkoutResource
    {
        $workout = $this->repository->update($request, $workout);

        return new WorkoutResource($workout->load('exercises.sets'));
    }

    public function destroy(Workout $workout): WorkoutResource
    {
        $workout->delete();

        return new WorkoutResource($workout);
    }
}
