<?php

namespace App\Http\Controllers;

use App\Exercise;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Throwable;

class ExerciseController extends Controller
{
    /**
     * @return Exercise[]|Collection
     */
    public function index()
    {
        return Exercise::all();
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return Exercise::create($request->all());
    }

    /**
     * @param  Request  $request
     * @param  Exercise  $exercise
     * @return bool
     */
    public function update(Request $request, Exercise $exercise)
    {
        return $exercise->update($request->all());
    }

    /**
     * @param  Exercise  $exercise
     * @return bool|null
     * @throws Throwable
     */
    public function destroy(Exercise $exercise)
    {
        return $exercise->delete();
    }
}
