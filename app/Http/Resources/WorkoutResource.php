<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name ?? '',
            'description' => $this->description ?? '',
            'rating' => $this->rating,
            'startedAt' => $this->started_at,
            'endedAt' => $this->ended_at,
            'exercises' => ExerciseResource::collection($this->whenLoaded('exercises')),
        ];
    }
}
