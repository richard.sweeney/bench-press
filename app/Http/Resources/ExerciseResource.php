<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ExerciseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'order' => $this->whenPivotLoaded('exercise_workout', function() {
                return $this->pivot->order;
            }),
            'sets' => $this->whenPivotLoaded('exercise_workout', function() {
                return SetResource::collection($this->pivot->sets);
            }),
        ];
    }
}
