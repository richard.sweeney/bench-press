<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ExerciseWorkout extends Pivot
{
    public $incrementing = true;

    protected $dates = ['created_at'];

    public function sets(): HasMany
    {
        return $this->hasMany(Set::class, 'exercise_workout_id', 'id')
            ->orderBy('order');
    }
}
