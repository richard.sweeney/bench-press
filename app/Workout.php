<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workout extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'started_at';

    protected $fillable = ['name', 'description', 'rating', 'user_id'];

    protected $dates = ['ended_at'];

    public function exercises(): BelongsToMany
    {
        return $this->belongsToMany(Exercise::class)
            ->using(ExerciseWorkout::class)
            ->withPivot([
                'id',
                'order',
            ])
            ->orderBy('order');
    }
}
