<?php

namespace App\Repositories;

use App\Http\Requests\AllWorkouts;
use App\Http\Requests\StoreWorkout;
use App\Workout;
use Carbon\Carbon;

class WorkoutRepository
{
    public function index(AllWorkouts $request)
    {
        $query = Workout::query();

        if ($request->has('user_ids')) {
            $query->whereIn('user_ids', $request->get('user_ids'));
        }

        if ($request->has('start_date')) {
            $query->where('created_at', '>=', Carbon::parse($request->get('start_date')));
        }

        if ($request->has('end_date')) {
            $query->where('created_at', '<=', Carbon::parse($request->get('end_date')));
        }

        if ($request->has('includes')) {
            $includes = explode(',', $request->get('includes'));

            $query->with($includes);
        }

        return $query->paginate();
    }

    public function create(StoreWorkout $request): Workout
    {
        $workout = Workout::create($request->toArray());

        return $this->syncExercises($workout, $request->get('exercises', []));
    }

    public function update(StoreWorkout $request, Workout $workout): Workout
    {
        $workout->update($request->all());

        return $this->syncExercises($workout, $request->get('exercises', []));
    }

    public function syncExercises(Workout $workout, array $exercises = null): Workout
    {
        $workout->exercises()->detach();

        foreach ($exercises as $exercise) {
            $workout->exercises()->attach($exercise['id'], [
                'order' => $exercise['order']
            ]);

            foreach ($exercise['sets'] as $sets) {
                $exerciseWorkout = $workout->exercises->find($exercise['id']);

                $exerciseWorkout->pivot->sets()->delete();
                $exerciseWorkout->pivot->sets()->createMany($sets);
            }
        }

        return $workout;
    }
}
