<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function workouts(): BelongsToMany
    {
        $this->belongsToMany(Workout::class)
            ->using(ExerciseWorkout::class)
            ->withPivot([
                'id',
                'order',
            ])
            ->orderBy('order');
    }

    public function sets(): HasManyThrough
    {
        return $this->hasManyThrough(
            Set::class,
            ExerciseWorkout::class,
            'exercise_id',
            'exercise_workout_id'
        );
    }
}
