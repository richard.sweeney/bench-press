<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Workout[] $workouts
 * @property-read int|null $workouts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Exercise
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Exercise onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Exercise whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Exercise withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Exercise withoutTrashed()
 */
	class Exercise extends \Eloquent {}
}

namespace App{
/**
 * App\ExerciseWorkout
 *
 * @property int $workout_id
 * @property int $exercise_id
 * @property int $reps
 * @property float $weight
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout whereExerciseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout whereReps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExerciseWorkout whereWorkoutId($value)
 */
	class ExerciseWorkout extends \Eloquent {}
}

namespace App{
/**
 * App\Workout
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float|null $rating
 * @property int $user_id
 * @property \Illuminate\Support\Carbon $started_at
 * @property string|null $ended_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Exercise[] $exercises
 * @property-read int|null $exercises_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Workout onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Workout whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Workout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Workout withoutTrashed()
 */
	class Workout extends \Eloquent {}
}

