<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('workouts')->group(function () {
    Route::get('/', 'WorkoutController@index');
    Route::post('/', 'WorkoutController@store');
    Route::get('/{workout}', 'WorkoutController@show');
    Route::put('/{workout}', 'WorkoutController@update');
    Route::delete('/{workout}','WorkoutController@destroy');
});
